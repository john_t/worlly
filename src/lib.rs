#![warn(clippy::pedantic)]

use std::collections::HashMap;

pub mod matches;
use matches::*;

#[must_use = "Calculating the result isn't awfully cheap"]
pub fn check_applicable_word(
    word: &str,
    chosen_word: &str,
    cows: &str,
    bulls: &str,
) -> bool {
    let mut applicable = true;

    // Removes words which contain unmatching letters
    for letter in chosen_word.chars() {
        if !(bulls.trim().contains(letter) || cows.trim().contains(letter)) {
            applicable &= !word.contains(letter);
        }
    }

    for (n, bull) in bulls.trim().chars().enumerate() {
        if bull == '*' {
            // Remove words which have a a letter in a spot which it is not.
            applicable &= word.chars().nth(n) != chosen_word.chars().nth(n);
        } else {
            // Remove words not containing out bulls.
            applicable &= word.chars().nth(n) == Some(bull);
        }
    }

    // Remove words which don't contain our cows
    for cow in cows.trim().chars() {
        applicable &= word.contains(cow);
    }

    applicable
}

pub fn compute_quantities<'a>(
    iter: impl Iterator<Item = &'a str>,
) -> Vec<HashMap<char, usize>> {
    let mut vec = Vec::new();
    for i in iter {
        for (pos, chr) in i.chars().enumerate() {
            while vec.get(pos) == None {
                vec.push(HashMap::new());
            }
            let hashmap = vec.get_mut(pos);
            if let Some(hashmap) = hashmap {
                hashmap.insert(chr, 1 + hashmap.get(&chr).unwrap_or(&0));
            }
        }
    }
    vec
}

pub fn compute_quantities_multiple_characters<'a>(
    iter: impl Iterator<Item = &'a str>,
) -> Vec<HashMap<&'a [u8], usize>> {
    let mut vec = Vec::new();
    for word in iter {

        // Loop through all the single letters, then two letters
        // etc.
        for winlength in 1..=word.len() {
            for (pos, needle) in word.as_bytes().windows(winlength).enumerate() {
                // Make sure the vec can reach this position.
                while vec.get(pos) == None {
                    vec.push(HashMap::new());
                }

                let hashmap = vec.get_mut(pos).unwrap();
                *hashmap.entry(needle).or_default() += 1;
            }
        }
    }
    vec
}

pub fn compute_quantities_unpositional<'a>(
    iter: impl Iterator<Item = &'a str>,
) -> HashMap<char, usize> {
    let mut hashmap = HashMap::new();
    for i in iter {
        for chr in i.chars() {
            hashmap.insert(chr, 1 + hashmap.get(&chr).unwrap_or(&0));
        }
    }
    hashmap
}

pub fn get_good_words_nocbr(
    vec: &mut Vec<&str>,
    word_scores: &[HashMap<char, usize, impl std::hash::BuildHasher>],
    nocbr: &[char],
) {
    vec.sort_unstable_by_key(|word| {
        let mut score = 0;
        let mut covered_chars = Vec::with_capacity(word.len());

        for (chr, hashmap) in word.chars().zip(word_scores.iter()) {
            if !covered_chars.contains(&chr) && !nocbr.contains(&chr) {
                score += hashmap.get(&chr).unwrap_or(&0).pow(2);
                covered_chars.push(chr);
            }
        }

        usize::MAX - score
    });
}

pub fn get_good_words_nocbr_unpositional(
    vec: &mut Vec<&str>,
    word_scores: &HashMap<char, usize, impl std::hash::BuildHasher>,
    nocbr: &[char],
) {
    vec.sort_unstable_by_key(|word| {
        let mut score = 0;
        let mut covered_chars = Vec::with_capacity(word.len());

        for chr in word.chars() {
            if !covered_chars.contains(&chr) && !nocbr.contains(&chr) {
                score += word_scores.get(&chr).unwrap_or(&0);
                covered_chars.push(chr);
            }
        }

        usize::MAX - score
    });
}

pub fn get_good_words(
    vec: &mut Vec<&str>,
    word_scores: &[HashMap<char, usize, impl std::hash::BuildHasher>],
) {
    vec.sort_unstable_by_key(|word| {
        let mut score = 0;
        let mut covered_chars = Vec::with_capacity(word.len());

        for (chr, hashmap) in word.chars().zip(word_scores.iter()) {
            if !covered_chars.contains(&chr) {
                score += hashmap.get(&chr).unwrap_or(&0);
                covered_chars.push(chr);
            }
        }

        usize::MAX - score
    });
}

pub fn get_good_words_multichar_nocbr(
    vec: &mut Vec<&str>,
    word_scores: &[HashMap<&[u8], usize, impl std::hash::BuildHasher>],
    nocbr: &[char],
) {
    vec.sort_unstable_by_key(|word| {
        let mut score = 0;

        for winlength in 1..=word.len() {
            'needle: for (pos, needle) in word.as_bytes().windows(winlength).enumerate() {

                for nocbr in nocbr {
                    if needle.contains(&nocbr.to_string().as_bytes()[0]) {
                        continue 'needle;
                    }
                }

                let hashmap = word_scores.get(pos).unwrap();
                score += hashmap.get(needle).unwrap_or(&0);
            }
        }

        usize::MAX - score
    });
}

pub fn get_good_words_multichar(
    vec: &mut Vec<&str>,
    word_scores: &[HashMap<&[u8], usize, impl std::hash::BuildHasher>],
) {
    vec.sort_unstable_by_key(|word| {
        let mut score = 0;

        for winlength in 1..=word.len() {
            for (pos, needle) in word.as_bytes().windows(winlength).enumerate() {
                let hashmap = word_scores.get(pos).unwrap();
                score += hashmap.get(needle).unwrap_or(&0) * (3 - winlength);
            }
        }

        usize::MAX - score
    });
}

pub fn get_good_words_unpositional(
    vec: &mut Vec<&str>,
    word_scores: &HashMap<char, usize, impl std::hash::BuildHasher>,
) {
    vec.sort_unstable_by_key(|word| {
        let mut score = 0;
        let mut covered_chars = Vec::with_capacity(word.len());

        for chr in word.chars() {
            if !covered_chars.contains(&chr) {
                score += word_scores.get(&chr).unwrap_or(&0);
                covered_chars.push(chr);
            }
        }

        usize::MAX - score
    });
}

pub fn get_good_words_matches_and_likelihood(
    vec: &mut Vec<&str>,
    word_scores: &[HashMap<char, usize, impl std::hash::BuildHasher>],
) {
    let vc = vec.clone();
    vec.sort_unstable_by_key(|word| {
        let mut score = 0;
        for answer in &vc {
            let matched = match_word(answer, word);
            score += get_score(matched);
        }
        
        usize::MAX - score
    })
}
