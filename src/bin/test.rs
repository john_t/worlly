#![warn(clippy::pedantic)]

use std::collections::HashMap;
use std::sync::{Arc, Mutex, atomic::{AtomicU64, Ordering}};
use rayon::prelude::*;

use worlly::*;

fn main() {
    let words = include_str!("../words.txt");
    let words: Vec<&str> = words.split('\n').collect();
    let per = Arc::new(AtomicU64::new(0));

    let mut hashmap: Arc<Mutex<HashMap<usize, usize>>> = Arc::new(Mutex::new(HashMap::new()));

    words.par_iter()
        .map(|word| {
            per.fetch_add(1, Ordering::Relaxed);
            println!("{:.2}%", 100.0 * ((per.load(Ordering::Relaxed) as f64) / (words.len() as f64)));
            println!("Testing word {}", word);

            let tries = test_word(word, words.clone());
            println!("Completed in {} tries", tries);

            tries
        }
    ).for_each(|tries| {
        let hashmap = hashmap.clone();
        let mut hashmap = hashmap.lock().expect("Found a poisoned mutex");
        let entry = hashmap.entry(tries);
        *entry.or_default() += 1;
        println!();
    });

    println!("{:#?}", hashmap);
}

fn test_word(word: &str, mut words: Vec<&str>) -> usize {
    let mut i = 1;
    let mut current_cows_or_bulls = Vec::new();

    loop {

        // Score the words based of how common each char is,
        // but not repeating.
        let quantities = compute_quantities_multiple_characters(words.iter().copied());

        // Get the best word
        get_good_words_multichar(&mut words, &quantities);
        let chosen_word = words[0];
        // dbg!(&chosen_word);

        // See what matched properly
        let bulls = get_bulls(word, chosen_word);

        // See what matched in the wrong placed.
        let cows = get_cows(word, chosen_word);

        current_cows_or_bulls.clear();

        if chosen_word == word {
            return i;
        }

        // Remove words which aren't going to be
        words.retain(|word| {
            check_applicable_word(word, chosen_word, &cows, &bulls)
        });

        current_cows_or_bulls.extend(cows.chars());
        current_cows_or_bulls.extend(bulls.chars());
        i += 1;
    }
}

/// Gets all of the bulls for a word.
fn get_bulls(word: &str, chosen_word: &str) -> String {
    let mut string = String::new();
    for (wordc, chosenc) in word.chars().zip(chosen_word.chars()) {
        if wordc == chosenc {
            string.push(wordc);
        } else {
            string.push('*');
        }
    }
    string
}

/// Get all of the cows for a word
fn get_cows(word: &str, chosen_word: &str) -> String {
    let mut string = String::new();
    for (wordc, chosenc) in word.chars().zip(chosen_word.chars()) {
        if wordc != chosenc && word.contains(chosenc) {
            string.push(chosenc);
        }
    }
    string
}
