#![warn(clippy::pedantic)]

use std::io::{self, stdin, stdout, Write};

use worlly::*;

fn main() -> io::Result<()> {
    let words = include_str!("../words.txt");
    let mut words: Vec<&str> = words.split('\n').collect();
    let stdin = stdin();
    let mut stdout = stdout();
    println!("How long is the word:");
    stdout.flush()?;

    // Get the length of the word
    let length: usize = loop {
        let mut buf = String::new();
        stdin.read_line(&mut buf)?;
        if let Ok(o) = buf.trim().parse() {
            break o;
        }
        println!("Failed to parse...");
    };
    words.retain(|x| x.len() == length);

    let mut current_cows_or_bulls = Vec::new();

    loop {
        // If we have under ten word sit is best to print them;
        // because at this point the user will probably just pick
        // the most common word.
        if words.len() < 10 {
            println!("Here are the words it may be:");
            for word in &words {
                println!(" - {}", word);
            }
        }

        // Score the words based of how common each char is,
        // but not repeating.
        let quantities = compute_quantities_multiple_characters(words.iter().copied());
        let mut chosen_word = String::new();

        // Get the best word
        get_good_words_multichar_nocbr(&mut words, &quantities, &current_cows_or_bulls);
        for good_word in &words {
            println!("Try the word: {}", good_word);

            // Check if the word was accepted
            print!("Accepted [y/n]? ");
            stdout.flush()?;
            let mut accepted = String::new();
            stdin.read_line(&mut accepted)?;
            if accepted.starts_with('y') {
                chosen_word = (*good_word).to_string();
                break;
            }
        }

        // See what matched properly
        println!("Which letters matched (* for wildcard character):");
        let mut bulls = String::new();
        stdin.read_line(&mut bulls)?;

        // See what matched in the wrong placed.
        println!("Which letters matched in the wrong place:");
        let mut cows = String::new();
        stdin.read_line(&mut cows)?;

        // Remove words which aren't going to be
        words.retain(|word| {
            check_applicable_word(word, &chosen_word, &cows, &bulls)
        });

        current_cows_or_bulls.extend(cows.chars());
        current_cows_or_bulls.extend(bulls.chars());
    }
}
