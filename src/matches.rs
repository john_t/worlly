#[derive(Copy, Clone, Eq, PartialEq)]
pub enum LetterMatchLevel {
    None,
    Partial,
    Full,
}

#[derive(Copy, Clone)]
pub struct LetterMatch {
    pub level: LetterMatchLevel,
    pub chr: char,
}

pub type MatchedWord = [LetterMatch; 5];

pub fn match_word(answer: &str, question: &str) -> MatchedWord {
    let mut matches = [LetterMatch { level: LetterMatchLevel::None, chr: '!' }; 5];
    for (pos, (achr, qchr)) in answer.chars().zip(question.chars()).enumerate() {
        // Check for full matches
        if qchr == achr {
            matches[pos].chr = achr;
            matches[pos].level = LetterMatchLevel::Full;
        } else if answer.contains(qchr) {
            matches[pos].chr = qchr;
            matches[pos].level = LetterMatchLevel::Partial;
        } else {
            matches[pos].chr = qchr;
            matches[pos].level = LetterMatchLevel::None;
        }
    }
    matches
}

pub fn get_score(mw: MatchedWord) -> usize {
    let mut score = 0;
    for i in mw {
        match i.level {
            LetterMatchLevel::Full => score += 5,
            LetterMatchLevel::Partial => score += 1,
            _ => (),
        }
    }
    score
}

pub fn completely_matched(mw: MatchedWord) -> bool {
    for i in mw {
        if i.level != LetterMatchLevel::Full {
            return false;
        }
    }

    true
}
